<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use App\Models\Permission;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'=>'admin',
            'email'=>'admin@admin.com',
            'password'=>bcrypt(123456)
        ]);

        $user->attachRole('super_admin');
        $user->attachPermissions([
            'create_users',
            'read_users',
            'update_users',
            'delete_users',
            'create_categories',
            'read_categories',
            'update_categories',
            'delete_categories',
            'create_products',
            'read_products',
            'update_products',
            'delete_products',
            'create_clients',
            'update_clients',
            'read_clients',
            'delete_clients',
            'create_orders',
            'read_orders',
            'update_orders',
            'delete_orders',
        ]);
    }
}
