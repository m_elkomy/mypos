<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{


    public function __construct()
    {
        $this->middleware(['permission:read_users'])->only('index');
        $this->middleware(['permission:create_users'])->only('create');
        $this->middleware(['permission:update_users'])->only('update');
        $this->middleware(['permission:delete_users'])->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::whereRoleIs('admin')->get();
        return view('admin.users.index',['title'=>trans('admin.Users List'),'users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create',['title'=>trans('admin.Add User')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'=>'required|min:6',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|confirmed|min:8',
            'password_confirmation'=>'required',
            'permissions'=>'required',
            'image'=>'image',
        ]);

        $data['password'] = bcrypt($request->password);
        if($request->image){
            Image::make($request->image)->resize(300,null,function($constraint){
                $constraint->aspectRatio();
            })->save(public_path('uploads/users/'.$request->image->hashName()));

            $data['image'] = $request->image->hashName();
        }
        $user = User::create($data);
        $user->attachRole('admin');
        $user->syncPermissions($request->permissions);
        session()->flash('success',trans('admin.Data Added Successfully'));
        return redirect(aurl('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit',['title'=>trans('admin.User Edit'),'user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $data = $request->validate([
            'name'=>'required|min:6',
            'email'=>'required|email|unique:users,email,'.$id,
            'image'=>'image',
        ]);

        if($request->image){
            if($user->image !='default.png'){
                Storage::disk('public_uploads')->delete('users/'.$user->image);
            }

            Image::make($request->image)->resize(300,null,function($constraint){
                $constraint->aspectRatio();
            })->save(public_path('uploads/users/'.$request->image->hashName()));

            $data['image'] = $request->image->hashName();
        }
        $user->update($data);
        $user->syncPermissions($request->permissions);
        session()->flash('success',trans('admin.Data Updated Successfully'));
        return redirect(aurl('users'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user->image != 'default.png'){
            Storage::disk('public_uploads')->delete('users/'.$user->image);
        }
        $user->delete();
        session()->flash('success',trans('admin.Data Deleted Successfully'));
        return redirect(aurl('users'));
    }
}
