<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('admin.category.index',['title'=>trans('admin.Categories List'),'categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create',['title'=>trans('admin.Add Category')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [];
        foreach(config('translatable.locales') as $local){
            $rules+=[$local.'.*'=>'required'];
            $rules+=[$local.'.name'=>[Rule::unique('category_translations','name')]];
        }
        $data = $request->validate($rules);

        Category::create($data);
        session()->flash('success',trans('admin.Data Added Successfully'));
        return redirect(aurl('categories'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.category.edit',['title'=>trans('admin.Category Edit'),'category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $rules = [];
        foreach(config('translatable.locales') as $local){
            $rules+=[$local.'.name'=>['required',Rule::unique('category_translations','name')->ignore($category->id,'category_id')]];
        }
        $data = $request->validate($rules);
        $category->update($data);
        session()->flash('success',trans('admin.Data Updated Successfully'));
        return redirect(aurl('categories'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        session()->flash('success',trans('admin.Data Deleted Successfully'));
        return redirect(aurl('categories'));
    }
}
