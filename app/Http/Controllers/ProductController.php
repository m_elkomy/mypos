<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $category = Category::all();
        return view('admin.product.index',['title'=>trans('admin.Products List'),'products'=>$products,'categories'=>$category]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.product.create',['title'=>trans('admin.Add Product'),'categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'category_id'=>'required|exists:category_translations,id',
            'purchase_price'=>'required',
            'sale_price'=>'required',
            'stock'=>'required',
            'image'=>'image',
        ];
        foreach(config('translatable.locales') as $local){
            $rules+=[$local.'.name'=>'required|unique:product_translations,name'];
            $rules+=[$local.'.description'=>'required'];
        }

        $data = $request->validate($rules);
        if($request->image){
            Image::make($request->image)->resize(300,null,function($constraint){
                $constraint->aspectRatio();
            })->save(public_path('uploads/products/'.$request->image->hashName()));

            $data['image'] = $request->image->hashName();
        }
        Product::create($data);
        session()->flash('success',trans('admin.Data Added Successfully'));
        return redirect(aurl('products'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();
        return view('admin.product.edit',['title'=>trans('admin.Product Edit'),'product'=>$product,'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $rules = [
            'category_id'=>'required|exists:category_translations,id',
            'purchase_price'=>'required',
            'sale_price'=>'required',
            'stock'=>'required',
            'image'=>'image',
        ];
        foreach(config('translatable.locales') as $local){
            $rules+=[$local.'.name'=>['required',Rule::unique('product_translations','name')->ignore($id,'product_id')]];
            $rules+=[$local.'.description'=>'required'];
        }

        $data = $request->validate($rules);
        if($request->image){
            if($product->image != 'default.png'){
                Storage::disk('public_uploads')->delete('products/'.$product->image);
            }
            Image::make($request->image)->resize(300,null,function($constraint){
                $constraint->aspectRatio();
            })->save(public_path('uploads/products/'.$request->image->hashName()));

            $data['image'] = $request->image->hashName();
        }
        $product->update($data);
        session()->flash('success',trans('admin.Data Updated Successfully'));
        return redirect(aurl('products'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if($product->image !='default.png'){
            Storage::disk('public_uploads')->delete('products/'.$product->image);
        }
        $product->delete();
        session()->flash('success',trans('admin.Data Deleted Successfully'));
        return redirect(aurl('products'));
    }
}
