<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();
        return view('admin.clients.index',['title'=>trans('admin.Clients List'),'clients'=>$clients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clients.create',['title'=>trans('admin.Add Client')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
        $data = $request->validate([
            'name'=>'required',
            'address'=>'required',
            'phone'=>'required|array|min:1',
            'phone.0'=>'required',
        ]);
        Client::create($data);
        session()->flash('success',trans('admin.Data Added Successfully'));
        return redirect(aurl('clients'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);
        return view('admin.clients.edit',['title'=>trans('admin.Client Edit'),'client'=>$client]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name'=>'required',
            'address'=>'required',
            'phone'=>'required|array|min:1',
            'phone.0'=>'required',
        ]);
        Client::where('id',$id)->update($data);
        session()->flash('success',trans('admin.Data Updated Successfully'));
        return redirect(aurl('clients'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        $client->delete();
        session()->flash('success',trans('admin.Data Deleted Successfully'));
        return redirect(aurl('clients'));
    }
}
