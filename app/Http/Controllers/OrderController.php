<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Client;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Client $client)
    {
        $categories = Category::with('products')->get();
        return view('admin.orders.create',['title'=>trans('admin.Add Order'),'client'=>$client,'categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Client $client)
    {
        $data = $request->validate([
            'products'=>'required|array',
//            'quantity'=>'required|array',
        ]);

        $order = $client->orders()->create([]);
        $total_price = 0;

        $order->products()->attach($request->product);

        foreach ($request->products as $id=>$quantity){
            $product=Product::findOrFail($id);
            $total_price+=$product->sale_price * $quantity['quantity'];
            $product->update([
                'stock'=>$product->stock - $quantity['quantity']
            ]);
        }

        $order->update([
            'total_price'=>$total_price
        ]);

        session()->flash('success',trans('admin.Data Added Successfully'));
        return redirect(aurl('order'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
