<?php

namespace App\Http\Controllers;

use App\Mail\UserResetPassword;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class UserAuthController extends Controller
{
    //function to return login view
    public function login(){
        return view('admin.login',['title'=>trans('admin.Login')]);
    }

    //function to handle admin login
    public function do_login(){
        $remember_me = request('remember_me') == 1 ? true : false;
        if(admin()->attempt(['email'=>request('email'),'password'=>request('password')],$remember_me)){
            session()->flash('success',trans('admin.You Are Logged In Successfully'));
            return redirect(aurl());
        }else{
            session()->flash('error',trans('admin.Sorry There Were Error In Login Information'));
            return redirect('login');
        }
    }

    //function to logout admin
    public function logout(){
        admin()->logout();
        session()->flash('success',trans('admin.You Are Logged Out Successfully'));
        return redirect('login');
    }

    public function forget_password(){
        return view('admin.forget_password',['title'=>trans('admin.Forgot Password?')]);
    }

    public function forget_password_post(){
        $user = User::where('email',request('email'))->first();
        if(!empty($user)){
            $token = app('auth.password.broker')->createToken($user);
            $data = DB::table('password_resets')->insert([
                'email'=>$user->email,
                'token'=>$token,
                'created_at'=>Carbon::now()
            ]);
        Mail::to($user->email)->send(new UserResetPassword(['data'=>$user,'token'=>$token]));
        session()->flash('success',trans('admin.Reset Link Sent Successfully'));
        return back();
        }else{
            session()->flash('error',trans('admin.Sorry There Were An Error'));
            return back();
        }
    }

    public function reset_password($token){
        $check_token = DB::table('password_resets')->where('token',$token)->where('created_at','>',Carbon::now()->subHours(2))->first();
        if(!empty($check_token)){
            return view('admin.reset_password',['title'=>trans('admin.Reset Password'),'data'=>$check_token]);
        }else{
            session()->flash('error',trans('admin.Sorry There Were An Error'));
            return redirect('forget/password');
        }
    }


    public function reset_password_final($token){
        $check_token = DB::table('password_resets')->where('token',$token)->where('created_at','>',Carbon::now()->subHours(2))->first();
        $this->validate(request(),[
            'email'=>'required|email|in:'.$check_token->email,
            'password'=>'required|confirmed|min:8',
            'password_confirmation'=>'required',
        ]);
        if(!empty($check_token)){
            $user = User::where('email',$check_token->email)->update([
                'email'=>$check_token->email,
                'password'=>bcrypt(request('password'))
            ]);

            DB::table('password_resets')->where('email',request('email'))->delete();
            admin()->attempt(['email'=>request('email'),'password'=>request('password')],true);
            session()->flash('success',trans('admin.Password Reset Successfully'));
            return redirect(aurl());
        }else{
            session()->flash('error',trans('admin.Sorry There Were An Error'));
            return redirect('forget/password');
        }
    }
}
