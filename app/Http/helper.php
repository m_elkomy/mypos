<?php
if(!function_exists('aurl')){
    function aurl($url=null){
        return url('dashboard/'.$url);
    }
}


if(!function_exists('admin')){
    function admin(){
        return auth()->guard('web');
    }
}


if(!function_exists('active_menu')){
    function active_menu($link){
        if(preg_match('/'.$link.'/i',request()->segment(3))){
            return ['aria-expanded=true','','show','active'];
        }else{
            return ['aria-expanded=false','collapsed','',''];
        }
    }
}

if(!function_exists('active_link')){
    function active_link($link){
        if(request()->is('en/dashboard/'.$link)){
            return ['active'];
        }else{
            return [''];
        }
    }
}
