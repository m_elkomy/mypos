<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
class Product extends Model implements TranslatableContract
{
    use HasFactory,Translatable;
    protected $guarded=[];
    protected $table = 'products';
    public $translatedAttributes = ['name','description'];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    protected $appends = ['image_path','profit_percent'];
    public function getImagePathAttribute(){
        return asset('uploads/products/'.$this->image);
    }

    public function getProfitPercentAttribute(){
        $profit = $this->sale_price - $this->purchase_price;
        $profit_precent = $profit * 100 / $this->purchase_price;
        return number_format($profit_precent,2);
    }

    public function orders(){
        return $this->belongsToMany(Order::class,'product_order');
    }
}
