<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){

    Route::get('/',function(){
        return redirect('login');
    });

    Route::get('login','UserAuthController@login')->name('login');
    Route::post('login','UserAuthController@do_login')->name('do_login');

    Route::get('forget/password','UserAuthController@forget_password')->name('forget_password');
    Route::post('forget/password','UserAuthController@forget_password_post')->name('forget_password_post');

    Route::get('reset/password/{token}','UserAuthController@reset_password')->name('reset_password');
    Route::post('reset/password/{token}','UserAuthController@reset_password_final')->name('reset_password_final');

    Route::group(['middleware'=>'auth'],function(){

        Route::group(['prefix'=>'dashboard'],function(){
            Route::get('/',function(){
                return view('admin.index',['title'=>trans('admin.Admin Panel')]);
            });


            Route::resource('users','UserController');
            Route::get('users/delete/{id}','UserController@destroy')->name('user_delete');

            Route::resource('categories','CategoryController');
            Route::get('categories/delete/{id}','CategoryController@destroy')->name('category_delete');


            Route::resource('products','ProductController');
            Route::get('products/delete/{id}','ProductController@destroy')->name('product_delete');

            Route::resource('clients','ClientController');
            Route::get('clients/delete/{id}','ClientController@destroy')->name('client_delete');

            Route::resource('clients.orders','OrderController');
            Route::get('clients.orders/delete/{id}','OrderController@destroy')->name('order_delete');

            Route::resource('orders','OrdersController');
            Route::get('orders/{order}/products','OrdersController@products')->name('orders.products');


            Route::any('logout','UserAuthController@logout')->name('logout');
        });
    });

});

