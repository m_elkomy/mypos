@extends('admin.home')
@section('content')

    <div class="row layout-top-spacing">
        <div class="col-lg-12 col-12  layout-spacing">
            @include('admin.layout.message')
            <div id="tabsIcons" class="col-lg-12 col-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>{{$title}}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content  icon-tab">
                        <form method="post" action="{{route('categories.update',$category->id)}}" >
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="put">
                            @foreach(config('translatable.locales') as $local)
                                <div class="form-group row  mb-4">
                                    <label for="name" class="col-sm-3 col-form-label col-form-label-sm">{{trans('admin.'.$local.'.name')}}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-control-sm" id="name" placeholder="{{trans('admin.'.$local.'.name')}}" name="{{$local}}[name]" value="{{$category->translate($local)->name}}">
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">

                                <button type="submit" class="btn btn-primary">{{trans('admin.Update')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
