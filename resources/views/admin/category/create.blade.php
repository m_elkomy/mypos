@extends('admin.home')
@section('content')

    <div class="row layout-top-spacing">
        <div class="col-lg-12 col-12  layout-spacing">
            @include('admin.layout.message')
            <div id="tabsIcons" class="col-lg-12 col-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>{{$title}}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content  icon-tab">
                        <form method="post" action="{{route('categories.store')}}">
                            {{csrf_field()}}

                                @foreach(config('translatable.locales') as $local)
                                <div class="form-group row  mb-4">
                                    <label for="name_ar" class="col-sm-3 col-form-label col-form-label-sm">{{trans('admin.'.$local.'.name')}}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-control-sm" id="name_ar" placeholder="{{trans('admin.'.$local.'.name')}}" name="{{$local}}[name]" value="{{old($local.'.name')}}">
                                    </div>
                                </div>
                                @endforeach


                            <div class="form-group">

                                <button type="submit" class="btn btn-primary">{{trans('admin.Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
