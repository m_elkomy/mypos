@extends('admin.home')
@section('content')

    <div class="row layout-top-spacing">
        <div class="col-lg-12 col-12  layout-spacing">
            @include('admin.layout.message')
            <div id="tabsIcons" class="col-lg-12 col-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>{{$title}}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content  icon-tab">
                        <form method="post" action="{{route('clients.store')}}">
                            {{csrf_field()}}

                            <div class="form-group row  mb-4">
                                <label for="name" class="col-sm-3 col-form-label col-form-label-sm">{{trans('admin.Name')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" id="name" placeholder="{{trans('admin.Name')}}" name="name" value="{{old('name')}}">
                                </div>
                            </div>


                            <div class="form-group row  mb-4">
                                <label for="address" class="col-sm-3 col-form-label col-form-label-sm">{{trans('admin.Address')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" id="address" placeholder="{{trans('admin.Address')}}" name="address" value="{{old('address')}}">
                                </div>
                            </div>


                            @for($i=0;$i<2;$i++)
                                <div class="form-group row  mb-4">
                                    <label for="phone" class="col-sm-3 col-form-label col-form-label-sm">{{trans('admin.Phone')}}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-control-sm" id="phone" placeholder="{{trans('admin.Phone')}}" name="phone[]" >
                                    </div>
                                </div>
                            @endfor


                            <div class="form-group">

                                <button type="submit" class="btn btn-primary">{{trans('admin.Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
