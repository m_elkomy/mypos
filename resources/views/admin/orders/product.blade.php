@extends('admin.home')
@section('content')

    <div class="row layout-top-spacing">
        <div class="col-lg-6 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div id="accordionBasic" class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>{{$title}}</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content">



                </div>
            </div>
        </div>

    </div>

@endsection
