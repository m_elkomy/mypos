@extends('admin.home')
@push('css')
    <link href="{{url('/')}}/admin/assets/css/components/tabs-accordian/custom-accordions.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/admin/assets/css/tables/table-basic.css" rel="stylesheet" type="text/css" />


@endpush
@section('content')

    <div class="row layout-top-spacing">
        <div class="col-lg-6 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div id="accordionBasic" class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>{{$title}}</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content">
                    <div id="toggleAccordion">
                            @foreach($categories as $category)
                            <div class="card">
                            <div class="card-header" id="headingOne1">
                                    <section class="mb-0 mt-0">
                                        <div role="menu" class="collapsed" data-toggle="collapse" data-target="#{{str_replace(' ','-',$category->name)}}" aria-expanded="true" aria-controls="defaultAccordionOne">
                                            {{$category->name}}<div class="icons"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg></div>
                                        </div>
                                    </section>
                                </div>

                                <div id="{{str_replace(' ','-',$category->name)}}" class="collapse" aria-labelledby="headingOne1" data-parent="#toggleAccordion">
                                    <div class="card-body">
                                        @if($category->products->count() > 0)
                                            <div class="table-responsive">

                                                <table class="table table-bordered table-hover table-striped mb-4">
                                                    <tr>
                                                        <td>{{trans('admin.Name')}}</td>
                                                        <td>{{trans('admin.Stock')}}</td>
                                                        <td>{{trans('admin.Price')}}</td>
                                                        <td>{{trans('admin.Add')}}</td>
                                                    </tr>
                                                    @foreach($category->products as $product)
                                                        <tr>
                                                            <td>{{$product->name}}</td>
                                                            <td>{{$product->stock}}</td>
                                                            <td>{{$product->sale_price}}</td>
                                                            <td>
                                                                <a href="" id="product-{{$product->id}}" data-name="{{$product->name}}"
                                                                   data-id="{{$product->id}}" data-price="{{$product->sale_price}}" class="btn btn-success add-product-btn">+</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        @else
                                            <div class="alert alert-danger">{{trans('admin.There Is No Products')}}</div>
                                        @endif

                                    </div>
                                </div>
                            </div>

                        @endforeach


                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-6 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div id="accordionBasic" class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>{{$title}}</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content">

                    <form method="post" action="{{route('clients.orders.store',$client)}}">
                        {{csrf_field()}}
                        <div class="table-responsive">
                            <table class="table table-bordered mb-4">
                                <thead>
                                <tr>
                                    <th>{{trans('admin.Product')}}</th>
                                    <th>{{trans('admin.Quantity')}}</th>
                                    <th>{{trans('admin.Price')}}</th>
                                    <th>{{trans('admin.Action')}}</th>
                                </tr>
                                </thead>
                                <tbody class="order-list">

                                </tbody>

                            </table>
                            <div class="">
                                {{trans('admin.Total')}} <span class="total-price">: 0</span>
                            </div>
                            <button class="btn btn-primary btn-block disabled" id="form-btn">{{trans('admin.Add Order')}}</button>
                        </div>
                    </form>


                </div>
            </div>
        </div>

    </div>

    @push('js')
        <script src="{{url('/')}}/admin/assets/js/components/ui-accordions.js"></script>
        <script>
            $(document).ready(function(){
                $('.add-product-btn').on('click',function (e){
                    e.preventDefault();
                    var name = $(this).data('name');
                    var id = $(this).data('id');
                    var price = $(this).data('price');
                    $(this).removeClass('btn-success').addClass('btn-default disabled');
                    var html = `
                        <tr>
                            <td>${name}</td>
                            <td><input type="number" name="products[${id}][quantity]" data-price="${price}" class="form-control quantity" min="1" value="1"></td>
                            <td class="product-price">${price}</td>
                            <td><button class="btn btn-danger del-product-btn" data-id="${id}">-</button></td>
                        </tr>
                    `;
                    $('.order-list').append(html);
                    total();
                });

                $('body').on('click','.disabled',function(e){
                    e.preventDefault();
                });

                $('body').on('click','.del-product-btn',function (e){
                    e.preventDefault();
                    var id = $(this).data('id');
                    $(this).closest('tr').remove();
                    $('#product-'+id).removeClass('btn-default disabled').addClass('btn-success');
                    total();
                });

                $('body').on('keyup change','.quantity',function (e){
                    var quantity = parseInt($(this).val());
                    var product_price = $(this).data('price');
                    $(this).closest('tr').find('.product-price').html(quantity * product_price);
                    total();
                })
            });


            function total(){
                var price = 0;
                $('.order-list .product-price').each(function(index){
                  price+= parseInt($(this).html());
                });
                $('.total-price').html(price);


                if(price>0){
                    $('#form-btn').removeClass('disabled')
                }else{
                    $('#form-btn').addClass('disabled');
                }
            }
        </script>
    @endpush
@endsection
