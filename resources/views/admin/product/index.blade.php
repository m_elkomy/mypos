@extends('admin.home')
@push('css')
    <link href="{{url('/')}}/admin/plugins/animate/animate.css" rel="stylesheet" type="text/css" />
    <script src="{{url('/')}}/admin/plugins/sweetalerts/promise-polyfill.js"></script>
    <link href="{{url('/')}}/admin/plugins/sweetalerts/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/admin/plugins/sweetalerts/sweetalert.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/admin/assets/css/components/custom-sweetalert.css" rel="stylesheet" type="text/css" />
@endpush
@section('content')
    <div class="row layout-top-spacing">
        @include('admin.layout.message')
        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
            <div class="widget-content widget-content-area br-6">
                <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                    <thead>
                    <tr>
                        <th>{{trans('admin.ID')}}</th>
                        <th>{{trans('admin.Image')}}</th>
{{--                        <th>{{trans('admin.Category')}}</th>--}}
                        <th>{{trans('admin.Name')}}</th>
                        <th>{{trans('admin.Description')}}</th>
                        <th>{{trans('admin.Purchase Price')}}</th>
                        <th>{{trans('admin.Sale Price')}}</th>
                        <th>{{trans('admin.Profit')}}</th>
                        <th>{{trans('admin.Stock')}}</th>
                        <th>{{trans('admin.Category')}}</th>
                        <th class="dt-no-sorting">{{trans('admin.Action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{$product->id}}</td>
                                <td><img src="{{$product->image_path}}" width="50px" height="50px" class="img-thumbnail"> </td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->description}}</td>
                                <td>{{$product->purchase_price}}</td>
                                <td>{{$product->sale_price}}</td>
                                <td>{{$product->profit_percent}}</td>
                                <td>{{$product->stock}}</td>
                                <td>
                                    @foreach($categories as $category)
                                        @if($category->id==$product->category_id)
                                            {{$category->name}}
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    @if(auth()->user()->hasPermission('update_products'))

                                        <a href="{{route('products.edit',$product->id)}}" class="btn btn-primary">{{trans('admin.Edit')}}</a>
                                    @else
                                        <a href="" class="disabled btn btn-primary">{{trans('admin.Edit')}}</a>
                                    @endif
                                    @if(auth()->user()->hasPermission('delete_products'))
                                        <a href="{{route('product_delete',$product->id)}}" class="btn btn-danger delbtn">{{trans('admin.Delete')}}</a>
                                    @else
                                        <a href="#" class="btn btn-danger disabled">{{trans('admin.Delete')}}</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    @push('js')
        <script src="{{url('/')}}/admin/plugins/sweetalerts/sweetalert2.min.js"></script>
        <script src="{{url('/')}}/admin/plugins/sweetalerts/custom-sweetalert.js"></script>
        <script>
            $('.delbtn').on('click', function (e) {
                e.preventDefault();
                var link = $(this).attr('href');
                const swalWithBootstrapButtons = swal.mixin({
                    confirmButtonClass: 'btn btn-success btn-rounded',
                    cancelButtonClass: 'btn btn-danger btn-rounded mr-3',
                    buttonsStyling: false,
                })

                swalWithBootstrapButtons({
                    title: '{{trans('admin.Are you sure?')}}',
                    text: "{{trans('admin.You wont be able to revert this!')}}",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{{trans('admin.Yes, delete it!')}}',
                    cancelButtonText: '{{trans('admin.No, cancel!')}}',
                    reverseButtons: true,
                    padding: '2em'
                }).then(function(result) {
                    if (result.value) {
                        window.location.href = link;
                    } else if (
                        // Read more about handling dismissals
                        result.dismiss === swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons(
                            '{{trans('admin.Cancelled')}}',
                            '{{trans('admin.Your imaginary file is safe :)')}}',
                            'error'
                        )
                    }
                })
            })
        </script>
        <!-- END THEME GLOBAL STYLE -->
    @endpush
@endsection
