@extends('admin.home')
@push('css')
    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="{{url('/')}}/admin/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/admin/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
@endpush
@section('content')

    <div class="row layout-top-spacing">
        <div class="col-lg-12 col-12  layout-spacing">
            @include('admin.layout.message')
            <div id="tabsIcons" class="col-lg-12 col-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>{{$title}}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content  icon-tab">
                        <form method="post" action="{{route('products.store')}}" enctype="multipart/form-data">
                            {{csrf_field()}}

                            <div class="form-group row mb-4">
                                <label for="categories" class="col-sm-3 col-form-label col-form-label-sm">{{trans('admin.Categories')}}</label>

                                <div class="col-sm-9">
                                    <select class="form-control form-control-lg" name="category_id">
                                        <option>{{trans('admin.Categories')}}</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}" {{old('category_id') == $category->id ? 'selected':''}}>{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @foreach(config('translatable.locales') as $local)
                                <div class="form-group row  mb-4">
                                    <label for="name_ar" class="col-sm-3 col-form-label col-form-label-sm">{{trans('admin.'.$local.'.name')}}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-control-sm" id="name_ar" placeholder="{{trans('admin.'.$local.'.name')}}" name="{{$local}}[name]" value="{{old($local.'.name')}}">
                                    </div>
                                </div>

                                <div class="form-group row  mb-4">
                                    <label for="description" class="col-sm-3 col-form-label col-form-label-sm">{{trans('admin.'.$local.'.description')}}</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="description" rows="3" name="{{$local}}[description]">{{old($local.'.description')}}</textarea>
                                    </div>
                                </div>
                            @endforeach

                            <div class="form-group row mb-4">
                                <label for="sale_price" class="col-sm-3 col-form-label col-form-label-lg">{{trans('admin.Sale Price')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" id="sale_price" name="sale_price" placeholder="{{trans('admin.Sale Price')}}" value="{{old('sale_price')}}">
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <label for="purchase_price" class="col-sm-3 col-form-label col-form-label-lg">{{trans('admin.Purchase Price')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" id="purchase_price" name="purchase_price" placeholder="{{trans('admin.Purchase Price')}}" value="{{old('purchase_price')}}">
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <label for="stock" class="col-sm-3 col-form-label col-form-label-lg">{{trans('admin.Stock')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" id="stock" name="stock" placeholder="{{trans('admin.Stock')}}" value="{{old('stock')}}">
                                </div>
                            </div>




                            <div class="form-group row mb-4">
                                <label for="image" class="col-sm-3 col-form-label col-form-label-lg">{{trans('admin.Image')}}</label>
                                <div class="col-sm-9">
                                    <input type="file" class="form-control-file image" id="image" name="image">
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <label for="image" class="col-sm-3 col-form-label col-form-label-lg">{{trans('admin.Image Preview')}}</label>
                                <div class="col-sm-9">
                                    <img src="{{asset('uploads/products/default.png')}}" width="100px" height="100px" class="img-thumbnail img-preview">
                                </div>
                            </div>

                            <div class="form-group">

                                <button type="submit" class="btn btn-primary">{{trans('admin.Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>




    @push('js')
        <script src="{{url('/')}}/admin/assets/js/scrollspyNav.js"></script>
        <script>
            $(".image").change(function() {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('.img-preview').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(this.files[0]); // convert to base64 string
                }
            });
        </script>
    @endpush
@endsection
