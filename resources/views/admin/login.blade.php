<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>{{isset($title) ? $title : trans('admin.Login')}}</title>
    <link rel="icon" type="image/x-icon" href="{{url('/')}}/admin/assets/img/favicon.ico"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    @if(app()->getLocale() == 'en')
        <link href="{{url('/')}}/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/admin/assets/css/plugins.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/admin/assets/css/authentication/form-2.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <link rel="stylesheet" type="text/css" href="{{url('/')}}/admin/assets/css/forms/theme-checkbox-radio.css">
        <link rel="stylesheet" type="text/css" href="{{url('/')}}/admin/assets/css/forms/switches.css">
    @else
        <link href="{{url('/')}}/admin/rtl/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/admin/rtl/assets/css/plugins.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/admin/rtl/assets/css/authentication/form-2.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <link rel="stylesheet" type="text/css" href="{{url('/')}}/admin/rtl/assets/css/forms/theme-checkbox-radio.css">
        <link rel="stylesheet" type="text/css" href="{{url('/')}}/admin/rtl/assets/css/forms/switches.css">
    @endif
    <!-- toastr -->
    <link href="{{url('/')}}/admin/plugins/notification/snackbar/snackbar.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
</head>
<body class="form">


    <div class="form-container outer">
        <div class="form-form">
            <div class="form-form-wrap">
                <div class="form-container">
                    <div class="form-content">

                        <h1 class="">{{trans('admin.Login')}}</h1>
                        <p class="">{{trans('admin.Log in to your account to continue.')}}</p>

                        <form class="text-left" method="post">
                            {{csrf_field()}}
                            <div class="form">
                                @include('admin.layout.message')
                                <div id="username-field" class="field-wrapper input">
                                    <label for="email">{{trans('admin.Email')}}</label>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                    <input id="email" name="email" type="email" class="form-control" placeholder="{{trans('admin.Email')}}">
                                </div>

                                <div id="password-field" class="field-wrapper input mb-2">
                                    <div class="d-flex justify-content-between">
                                        <label for="password">{{trans('admin.Password')}}</label>
                                        <a href="{{route('forget_password')}}" class="forgot-pass-link">{{trans('admin.Forgot Password?')}}</a>
                                    </div>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>
                                    <input id="password" name="password" type="password" class="form-control" placeholder="{{trans('admin.Password')}}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" id="toggle-password" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                </div>

                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="remember_me" name="remember_me" value="1">
                                        <label class="custom-control-label" for="remember_me">{{trans('admin.Remember Me')}}</label>
                                    </div>
                                </div>
                                <div class="d-sm-flex justify-content-between">
                                    <div class="field-wrapper">
                                        <button type="submit" class="btn btn-primary" value="">{{trans('admin.Login')}}</button>
                                    </div>
                                </div>

                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="{{url('/')}}/admin/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="{{url('/')}}/admin/bootstrap/js/popper.min.js"></script>
    @if(app()->getLocale() == 'en')
        <script src="{{url('/')}}/admin/bootstrap/js/bootstrap.min.js"></script>

        <!-- END GLOBAL MANDATORY SCRIPTS -->
        <script src="{{url('/')}}/admin/assets/js/authentication/form-2.js"></script>
    @else
        <script src="{{url('/')}}/admin/rtl/bootstrap/js/bootstrap.min.js"></script>

        <!-- END GLOBAL MANDATORY SCRIPTS -->
        <script src="{{url('/')}}/admin/rtl/assets/js/authentication/form-2.js"></script>
    @endif
    <!-- toastr -->
    <script src="{{url('/')}}/admin/plugins/notification/snackbar/snackbar.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!--  BEGIN CUSTOM SCRIPTS FILE  -->
    <script src="{{url('/')}}/admin/assets/js/components/notification/custom-snackbar.js"></script>
    <!--  END CUSTOM SCRIPTS FILE  -->

    <script>
        // Get the Toast button
        var toastButton = document.getElementById("toast-btn");
        // Get the Toast element
        var toastElement = document.getElementsByClassName("toast")[0];

        toastButton.onclick = function() {
            $('.toast').toast('show');
        }


    </script>
@stack('js')
</body>

</html>
