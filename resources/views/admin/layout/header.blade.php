<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>{{isset($title) ? $title : trans('admin.Admin Panel')}}</title>
    <link rel="icon" type="image/x-icon" href="{{url('/')}}/admin/assets/img/favicon.ico"/>
    <link href="{{url('/')}}/admin/assets/css/loader.css" rel="stylesheet" type="text/css" />
    <script src="{{url('/')}}/admin/assets/js/loader.js"></script>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    @if(app()->getLocale() == 'en')
        <link href="{{url('/')}}/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/admin/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    @else
        <link href="{{url('/')}}/admin/rtl/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/admin/rtl/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    @endif
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="{{url('/')}}/admin/plugins/apex/apexcharts.css" rel="stylesheet" type="text/css">
    @if(app()->getLocale() =='en')
        <link href="{{url('/')}}/admin/assets/css/dashboard/dash_1.css" rel="stylesheet" type="text/css" />
    @else
        <link href="{{url('/')}}/admin/rtl/assets/css/dashboard/dash_1.css" rel="stylesheet" type="text/css" />
    @endif
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <!-- toastr -->
    <link href="{{url('/')}}/admin/plugins/notification/snackbar/snackbar.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/admin/plugins/table/datatable/datatables.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/admin/plugins/table/datatable/custom_dt_html5.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/admin/plugins/table/datatable/dt-global_style.css">
    <!-- END PAGE LEVEL CUSTOM STYLES -->
    @stack('css')
</head>
<body data-spy="scroll" data-target="#navSection" data-offset="100">
