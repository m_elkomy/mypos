@if(session()->has('success'))
    @push('js')
        <script>
            Snackbar.show({
                text: '{{session('success')}}',
                actionTextColor: '#fff',
                backgroundColor: '#1abc9c',
                pos: 'top-right'
            });
        </script>
    @endpush
@endif

@if(session()->has('error'))
    @push('js')
        <script>
            Snackbar.show({
                text: '{{session('error')}}',
                actionTextColor: '#fff',
                backgroundColor: '#e7515a',
                pos: 'top-right',
            });
        </script>
    @endpush
@endif

@if($errors->all())
<div class="alert alert-danger">
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
</div>
@endif
