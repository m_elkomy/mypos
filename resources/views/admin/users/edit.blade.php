@extends('admin.home')
@push('css')
    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="{{url('/')}}/admin/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/admin/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
@endpush
@section('content')

    <div class="row layout-top-spacing">
        <div class="col-lg-12 col-12  layout-spacing">
            @include('admin.layout.message')
            <div id="tabsIcons" class="col-lg-12 col-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>{{$title}}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content  icon-tab">
                        <form method="post" action="{{route('users.update',$user->id)}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="put">
                            <div class="form-group row  mb-4">
                                <label for="name" class="col-sm-3 col-form-label col-form-label-sm">{{trans('admin.Name')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" id="name" placeholder="{{trans('admin.Name')}}" name="name" value="{{$user->name}}">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="email" class="col-sm-3 col-form-label">{{trans('admin.Email')}}</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" id="email" placeholder="{{trans('admin.Email')}}" name="email" value="{{$user->email}}">
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <label for="image" class="col-sm-3 col-form-label col-form-label-lg">{{trans('admin.Image')}}</label>
                                <div class="col-sm-9">
                                    <input type="file" class="form-control-file image" id="image" name="image">
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <label for="image" class="col-sm-3 col-form-label col-form-label-lg">{{trans('admin.Image Preview')}}</label>
                                <div class="col-sm-9">
                                    <img src="{{$user->image_path}}" width="100px" height="100px" class="img-thumbnail img-preview">
                                </div>
                            </div>

                            <div class="form-group">
                                @php
                                    $models = ['users','categories','products'];
                                    $maps = ['create','read','update','delete'];
                                @endphp
                                <ul class="nav nav-tabs  mb-3 mt-3" id="iconTab" role="tablist">
                                    @foreach($models as $index=>$model)
                                        <li class="nav-item">
                                            <a class="nav-link {{$index==0 ? 'active':''}}" id="icon-{{$model}}-tab" data-toggle="tab" href="#icon-{{$model}}" role="tab" aria-controls="icon-{{$model}}" aria-selected="true"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                                                {{$model}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content" id="iconTabContent-1">
                                    @foreach($models as $index=>$model)
                                        <div class="tab-pane fade {{$index==0?'show active':''}}" id="icon-{{$model}}" role="tabpanel" aria-labelledby="icon-{{$model}}-tab">
                                            @foreach($maps as $map)
                                                <div class="custom-control custom-checkbox d-inline">
                                                    <input type="checkbox" class="custom-control-input" {{$user->hasPermission($map.'_'.$model) ? 'checked':''}} id="{{$map . '_' .$model}}" name="permissions[]" value="{{$map.'_'.$model}}">
                                                    <label class="custom-control-label" for="{{$map.'_'.$model}}">{{trans('admin.'.$map)}}</label>
                                                </div>
                                            @endforeach

                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">

                                <button type="submit" class="btn btn-primary">{{trans('admin.Update')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>




    @push('js')
        <script src="{{url('/')}}/admin/assets/js/scrollspyNav.js"></script>
        <script>
            $(".image").change(function() {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('.img-preview').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(this.files[0]); // convert to base64 string
                }
            });
        </script>
    @endpush
@endsection
