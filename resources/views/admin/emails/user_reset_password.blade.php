@component('mail::message')
{{trans('admin.Welcome') .' ' . $data['data']->name}}

{{trans('admin.To Reset Your Password Click The Button Below')}}

@component('mail::button', ['url' => url('reset/password/'.$data['token'])])
{{trans('admin.Click Here')}}
@endcomponent

{{trans('admin.Thanks')}},<br>
{{ config('app.name') }}
@endcomponent
